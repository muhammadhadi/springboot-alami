package com.alami.demo.dto;

import java.sql.Date;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;
import javax.validation.constraints.Size;

public class UserData {

    @NotEmpty(message = "name cannot be empty")
    @Size(min = 3,message = "Name Cannot be less than 3 characters")
    private String user_name;

    @NotEmpty(message = "address cannot be empty")
    private String user_address;
    
    @NotNull(message = "Date cannot be empty")
    @Past(message = "birthdate cannot be in the future")
    private Date user_birthdate;

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public String getUser_address() {
        return user_address;
    }

    public void setUser_address(String user_address) {
        this.user_address = user_address;
    }

    public Date getUser_birthdate() {
        return user_birthdate;
    }

    public void setUser_birthdate(Date user_birthdate) {
        this.user_birthdate = user_birthdate;
    }

    

    
}
