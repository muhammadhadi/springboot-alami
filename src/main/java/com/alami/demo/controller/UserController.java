package com.alami.demo.controller;

import com.alami.demo.dto.ResponseData;
import com.alami.demo.dto.UserData;
import com.alami.demo.entities.User;
import com.alami.demo.services.UserService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

import javax.validation.Valid;

@RestController
@RequestMapping("/user")
public class UserController {
    
    @Autowired
    UserService userService;

    @GetMapping("sa")
    public String index(){
        return "halo ngab";
    }

    @GetMapping("getusers")
    public ResponseEntity<ResponseData<List<User>>> getUser(){
        
        ResponseData<List<User>> responseData = new ResponseData<>();

        // if(errors.hasErrors()){
        //     for (ObjectError error : errors.getAllErrors()) {
                
        //         responseData.getMessages().add(error.getDefaultMessage());
        //     }

        //     responseData.setStatus(false);
        //     responseData.setPayload(null);
        //     return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(responseData);
        // }

        List<User> userList = userService.getUsers(); 
        responseData.setStatus(true);
        responseData.setPayload(userList);
        // responseData.getMessages().add("User Successfully inserted");
        return ResponseEntity.ok(responseData);
        // return userService.getUsers();
    }

    @PostMapping("adduser")
    public ResponseEntity<ResponseData<User>> saveUser(@Valid @RequestBody UserData userData, Errors errors){
        
        ResponseData<User> responseData = new ResponseData<>();

        if(errors.hasErrors()){
            for (ObjectError error : errors.getAllErrors()) {
                
                responseData.getMessages().add(error.getDefaultMessage());
            }

            responseData.setStatus(false);
            responseData.setPayload(null);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(responseData);
        }

        User user = new User();
        user.setUser_name(userData.getUser_name());
        user.setUser_address(userData.getUser_address());
        user.setUser_birthdate(userData.getUser_birthdate());

        responseData.setStatus(true);
        responseData.setPayload(userService.saveUser(user));
        // responseData.getMessages().add("User Successfully inserted");
       return ResponseEntity.ok(responseData);
    }
    
    
}
