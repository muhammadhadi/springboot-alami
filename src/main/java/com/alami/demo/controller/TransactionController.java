package com.alami.demo.controller;

import java.time.LocalDate;
import java.util.List;

import javax.validation.Valid;

import com.alami.demo.dto.ResponseData;
import com.alami.demo.entities.Transaction;
import com.alami.demo.services.TransactionService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/transaction")
public class TransactionController {

    @Autowired
    TransactionService transactionService;

    @GetMapping("transaction")
    public String index(){
        return "home transaction";
    }

    @GetMapping("alltransaction")
    public ResponseEntity<ResponseData<List<Transaction>>> getAllTransaction(){

        ResponseData<List<Transaction>> responseData = new ResponseData<>();

        // responseData.getMessages().add("Transaction has succesfully inserted");
        responseData.setStatus(true);
        responseData.setPayload(transactionService.getTransactions());
        return ResponseEntity.ok(responseData);

        // return transactionService.getTransactions();
    }

    @PostMapping("addtransaction")
    public ResponseEntity<ResponseData<Transaction>> saveTransaction(@Valid @RequestBody Transaction transaction, Errors errors){
        
        ResponseData<Transaction> responseData = new ResponseData<>();
        
        if(errors.hasErrors()){
            for (ObjectError error : errors.getAllErrors()) {
                responseData.getMessages().add(error.getDefaultMessage());
            }
            
            responseData.setStatus(false);
            responseData.setPayload(null);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(responseData);
        }
        // responseData.getMessages().add("Transaction has succesfully inserted");
        responseData.setStatus(true);
        responseData.setPayload(transactionService.saveTransaction(transaction));
        return ResponseEntity.ok(responseData);
    }

    @GetMapping("between/{start}/{end}")
    public ResponseEntity<ResponseData<List<Transaction>>> transactionBetweenDate(@PathVariable(value = "start") @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate startDate,@PathVariable(name = "end") @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate endDate){
        
        ResponseData<List<Transaction>> responseData = new ResponseData<>();
        
        // if(errors.hasErrors()){
        //     for (ObjectError error : errors.getAllErrors()) {
        //         responseData.getMessages().add(error.getDefaultMessage());
        //     }
            
        //     responseData.setStatus(false);
        //     responseData.setPayload(null);
        //     return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(responseData);
        // }
        // responseData.getMessages().add();
        responseData.setStatus(true);
        responseData.setPayload(transactionService.findBetweenDate(startDate, endDate));
        return ResponseEntity.ok(responseData);

        // return transactionService.findBetweenDate(startDate, endDate);
    }

    @GetMapping("/{userId}")
    public ResponseEntity<ResponseData<List<Transaction>>> transactionByUserId(@PathVariable(value = "userId") Integer userId){
        ResponseData<List<Transaction>> responseData = new ResponseData<>();
        
        // if(errors.hasErrors()){
        //     for (ObjectError error : errors.getAllErrors()) {
        //         responseData.getMessages().add(error.getDefaultMessage());
        //     }
            
        //     responseData.setStatus(false);
        //     responseData.setPayload(null);
        //     return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(responseData);
        // }
        // responseData.getMessages().add();
        responseData.setStatus(true);
        responseData.setPayload(transactionService.findByTransactionUser(userId));
        return ResponseEntity.ok(responseData);
        
        // return transactionService.findByTransactionUser(userId);
    }
    
}
