package com.alami.demo.repositories;

import java.time.LocalDate;
import java.util.List;

import javax.validation.constraints.Past;

import com.alami.demo.entities.Transaction;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface TransactionRepository extends JpaRepository<Transaction,Integer>{
    @Query(nativeQuery = true,value = "select * from transaction where transaction_time between :startDate and :endDate")
    List<Transaction> findBetweenTransactionTime(@Param("startDate") @Past(message = "Transaction time cannot be in the future") LocalDate startDate, @Param("endDate") @Past(message = "Transaction time cannot be in the future") LocalDate endDate);

    List<Transaction> findByTransactionUser(Integer transactionUser);
}
