package com.alami.demo.services;

import java.time.LocalDate;
import java.util.List;

import com.alami.demo.entities.Transaction;
import com.alami.demo.repositories.TransactionRepository;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class TransactionService {
    
    @Autowired TransactionRepository transactionRepository;
    public TransactionService(){

    }

    public List<Transaction> getTransactions(){

        return transactionRepository.findAll();
    }

    public Transaction saveTransaction(Transaction transaction ){
        
        return transactionRepository.save(transaction);
    }

    public List<Transaction> findBetweenDate(LocalDate startDate, LocalDate endDate){
        return transactionRepository.findBetweenTransactionTime(startDate, endDate);
    }

    public List<Transaction> findByTransactionUser(Integer userId){
        return transactionRepository.findByTransactionUser(userId);
    }
}
