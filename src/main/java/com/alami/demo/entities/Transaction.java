package com.alami.demo.entities;

import java.io.Serializable;
import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Positive;


@Entity
@Table(name = "transaction",schema = "public")
public class Transaction implements Serializable{

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Pattern(regexp = "^(USER_TRANSACTION_STORE|USER_TRANSACTION_LEND|USER_TRANSACTION_PAY|USER_TRANSACTION_TAKE)$",message = "Invalid Transaction Type, Must between USER_TRANSACTION_STORE / USER_TRANSACTION_LEND / USER_TRANSACTION_PAY / USER_TRANSACTION_TAKE")
    @NotEmpty(message = "Transaction type is required")
    @Column(name = "transaction_type")
    public String transactionType;

    @Past(message = "Transaction time cannot be in the future")
    @NotNull(message = "Transaction time is required")
    @Column(name = "transaction_time")
    public Date transactionTime;

    @NotNull(message = "Transaction user id is required")
    @Column(name = "transaction_user_id")
    public Integer transactionUser;

    @NotNull(message = "Transaction amount is required")
    @Positive(message = "Transaction ammount cannot be negative value")
    @Column(name = "transaction_amount")
    public Integer transactionAmount;

    public Transaction(){

    }
    
    public Transaction(Long id,
            @Pattern(regexp = "^(USER_TRANSACTION_STORE|USER_TRANSACTION_LEND|USER_TRANSACTION_PAY|USER_TRANSACTION_TAKE)$", message = "Invalid Transaction Type, Must between USER_TRANSACTION_STORE / USER_TRANSACTION_LEND / USER_TRANSACTION_PAY / USER_TRANSACTION_TAKE") @NotEmpty(message = "Transaction type is required") String transactionType,
            @Past(message = "Transaction time cannot be in the future") @NotNull(message = "Transaction time is required") Date transactionTime,
            @NotNull(message = "Transaction user id is required") Integer transactionUser,
            @NotNull(message = "Transaction amount is required") @Positive(message = "Transaction ammount cannot be negative value") Integer transactionAmount) {
        this.id = id;
        this.transactionType = transactionType;
        this.transactionTime = transactionTime;
        this.transactionUser = transactionUser;
        this.transactionAmount = transactionAmount;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(String transactionType) {
        this.transactionType = transactionType;
    }

    public Date getTransactionTime() {
        return transactionTime;
    }

    public void setTransactionTime(Date transactionTime) {
        this.transactionTime = transactionTime;
    }

    public Integer getTransactionUser() {
        return transactionUser;
    }

    public void setTransactionUser(Integer transactionUser) {
        this.transactionUser = transactionUser;
    }

     
    public Integer getTransactionAmount() {
        return transactionAmount;
    }

    public void setTransactionAmount(Integer transactionAmount) {
        this.transactionAmount = transactionAmount;
    }

    
    
}
