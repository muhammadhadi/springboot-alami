package com.alami.demo;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.anyLong;

import java.sql.Date;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import javax.validation.constraints.Null;

import com.alami.demo.entities.Transaction;
import com.alami.demo.entities.User;
import com.alami.demo.services.TransactionService;
import com.alami.demo.services.UserService;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.skyscreamer.jsonassert.JSONAssert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.MockMvcPrint;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@SpringBootTest
@AutoConfigureMockMvc
class DemoApplicationTests {

	@Autowired 
	private MockMvc mvc;

	@MockBean
	private UserService userService;

	@MockBean
	private TransactionService transactionService;

	@Test
	public void adduser() throws Exception {
		long userId = 1;
		User mockUser = new User(userId,"testuser","testaddress",Date.valueOf("1990-10-01"));
		
		String mockUserJson = "{\"id\":1,\"user_name\":\"testuser\",\"user_address\":\"testaddress\",\"user_birthdate\":\"1990-10-01\"}";
		Mockito.when(userService.saveUser(Mockito.any(User.class))).thenReturn(mockUser);

		RequestBuilder requestBuilder = MockMvcRequestBuilders.post("http://localhost:8080/user/adduser")
		.accept(MediaType.APPLICATION_JSON).content(mockUserJson).contentType(MediaType.APPLICATION_JSON);

		MvcResult res = mvc.perform(requestBuilder).andReturn();
		System.out.println(res.getResponse());
		MockHttpServletResponse response = res.getResponse();

		String expected ="{status:true,messages:[],payload:{id:1,user_name:testuser,user_address:testaddress,user_birthdate:1990-10-01}}";
		assertEquals(HttpStatus.OK.value(), response.getStatus());

		JSONAssert.assertEquals(expected, res.getResponse()
				.getContentAsString(), false);
	}

	@Test
	public void getUser() throws Exception {
		RequestBuilder requestBuilder = MockMvcRequestBuilders.get("http://localhost:8080/user/getusers");
		this.mvc.perform(requestBuilder).andExpect(MockMvcResultMatchers.status().isOk());
	}

	@Test
	public void addtransaction() throws Exception {
		long transId = 100;
		Transaction mockTransaction = new Transaction(transId,"USER_TRANSACTION_STORE",Date.valueOf("2021-03-26"),1,100000);
		
		String mockTransactionJson = "{\"id\":100,\"transactionType\":\"USER_TRANSACTION_STORE\",\"transactionTime\":\"2021-03-26\",\"transactionUser\":1,\"transactionAmount\":100000}";
		Mockito.when(transactionService.saveTransaction(Mockito.any(Transaction.class))).thenReturn(mockTransaction);
		RequestBuilder requestBuilder = MockMvcRequestBuilders.post("http://localhost:8080/transaction/addtransaction")
		.accept(MediaType.APPLICATION_JSON).content(mockTransactionJson).contentType(MediaType.APPLICATION_JSON);

		MvcResult res = mvc.perform(requestBuilder).andReturn();
		System.out.println(res.getResponse());
		MockHttpServletResponse response = res.getResponse();
		String expected ="{status:true,messages:[],payload:{id:100,transactionType:USER_TRANSACTION_STORE,transactionTime:2021-03-26,transactionUser:1,transactionAmount:100000}}";
		assertEquals(HttpStatus.OK.value(), response.getStatus());
		JSONAssert.assertEquals(expected, res.getResponse()
				.getContentAsString(), false);
		
	}

	
	@Test
	public void getUserTransactionHistory() throws Exception {
		long transId = 10;
		List<Transaction> mockTransaction = new ArrayList<>();

		Transaction transaction1 = new Transaction(transId,"USER_TRANSACTION_STORE",Date.valueOf("2021-10-01"),1,100000);
		mockTransaction.add(transaction1);

		String mockTransactionJson = "{\"transactionType\":\"USER_TRANSACTION_STORE\",\"transactionTime\":\"2021-10-01\",\"transactionUser\":1,\"transactionAmount\":100000}";
		Mockito.when(transactionService.findByTransactionUser(10)).thenReturn(mockTransaction);
		RequestBuilder requestBuilder = MockMvcRequestBuilders.get("http://localhost:8080/transaction/10")
		.accept(MediaType.APPLICATION_JSON);

		MvcResult res = mvc.perform(requestBuilder).andReturn();
		System.out.println(res.getResponse());
		MockHttpServletResponse response = res.getResponse();
		String expected ="{status:true,messages:[],payload:[{id:10,transactionType:USER_TRANSACTION_STORE,transactionTime:2021-10-01,transactionUser:1,transactionAmount:100000}]}";
		assertEquals(HttpStatus.OK.value(), response.getStatus());
		JSONAssert.assertEquals(expected, res.getResponse()
				.getContentAsString(), false);
	}
	@Test
	public void transactionBetweenDate() throws Exception{
		long transId = 10;
		long transId2 = 11;
		List<Transaction> mockTransactions = new ArrayList<>();

		Transaction mockTransaction = new Transaction(transId,"USER_TRANSACTION_LEND",Date.valueOf("2021-02-01"),1,100000);
		Transaction mockTransaction2 = new Transaction(transId2,"USER_TRANSACTION_LEND",Date.valueOf("2021-03-01"),1,100000);
		mockTransactions.add(mockTransaction);
		mockTransactions.add(mockTransaction2);

		String mockTransactionJson = "{\"id\":10\",transactionType\":\"USER_TRANSACTION_LEND\",\"transactionTime\":\"2021-02-01\",\"transactionUser\":1,\"transactionAmount\":100000},{\"id\":11\",transactionType\":\"USER_TRANSACTION_LEND\",\"transactionTime\":\"2021-03-01\",\"transactionUser\":1,\"transactionAmount\":100000}";
		
		System.out.println(LocalDate.of(2021, 02, 01));
		Mockito.when(transactionService.findBetweenDate(LocalDate.of(2021, 02, 01),LocalDate.of(2021, 03, 26))).thenReturn(mockTransactions);
		
		RequestBuilder requestBuilder = MockMvcRequestBuilders.get("http://localhost:8080/transaction/between/2021-02-01/2021-03-26")
		.accept(MediaType.APPLICATION_JSON);
		
		MvcResult res = mvc.perform(requestBuilder).andReturn();
		System.out.println(res.getResponse());
		String expected = "{status:true,messages:[],payload:[{id:10,transactionType:USER_TRANSACTION_LEND,transactionTime:2021-02-01,transactionUser:1,transactionAmount:100000},{id:11,transactionType:USER_TRANSACTION_LEND,transactionTime:2021-03-01,transactionUser:1,transactionAmount:100000}]}";
		MockHttpServletResponse response = res.getResponse();

		assertEquals(HttpStatus.OK.value(), response.getStatus());
		JSONAssert.assertEquals(expected, res.getResponse()
				.getContentAsString(), false);
		
	}

	@Test
	public void getTransaction() throws Exception{
		RequestBuilder requestBuilder = MockMvcRequestBuilders.get("http://localhost:8080/transaction/alltransaction");
		this.mvc.perform(requestBuilder).andExpect(MockMvcResultMatchers.status().isOk());
	}

}
